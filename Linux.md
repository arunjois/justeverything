#Linux Commands

#Process taking too much time
ps -eLF

smartctl -t long /dev/sda4


#Find Folder
find . -type d -name "dir-name-here"
find . -type f -name "autoload_psr4.php"
find . -type f -mmin +30 -maxdepth 2 -print    
find . -type f -name '.json'
find /root/Maildir/ -mindepth 1 -type f -mtime +14 | xargs rm
#rm argument is too long
find . -name "*.pdi" -print0 | xargs -0 rm 

#For loop bash
`for i in $(ls -l | awk '{print $9}') ; do cd $i; cd .. ; done`

#To Clear RAM of cache
free -h;sysctl -w vm.drop_caches=3;free -h


#Search and replace vim
:%s/foo/bar/g

#Open http on 3000 port
firewall-cmd --zone=public --add-port=3000/tcp --permanent


#SHAsum of line in a file
while read line; do printf %s "$line" | tr -d '\r\n' | shasum; done

#Ulimit usage
ulimit -n
ulimit -Sa 
ulimit -has

#Disk Check 
smartctl -a /dev/sdX
smartctl -a /dev/sda4

#Nmap
sudo nmap -sn 192.168.1.0/24


#Kill process using kill command
kill -9 <pid>


#set acl from other file or dir
getfacl ind.svg | setfacl --set-file=- AUD.svg
#Copy ACL
getfacl bookmy-show.png.svg | setfacl --set-file=-BG-link-icon.

#Add up a column of numbers
alias sum="paste -sd+ -| bc "

#TAR a dir
tar -cvf  etc.tar    /etc/
tar -cvfz etc.tar.gz /etc/

#UNTAR file
tar -xvfz etc.tar.gz -C /path/of/dir
